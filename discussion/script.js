//Basic array structure
//Access elements in an Array - through index

/* Two ways to initialize an array  
  1. variable
  2. new keyword and array constructor
*/

let array = [1, 2, 3];
console.log(array);
let arr = new Array(1, 2, 3);
console.log(arr);

// console.log(array[0]);
// console.log(array[1]);
// console.log(array[2]);

// index = array.length - 1

/* 
  Array Manipulation 
  - changing array data
  - adding a new element at the end of an array 
      using assignment operator 
        can't add a new element to the beginning - will replace the element in that case 

  - Array methods 
        Push method
          adds an element at the end of the array 
          syntax: array.push()
        Pop method 
          removes last element of an array 
          syntax: array.pop()
        Unshift method 
          adds an element at the beginning of an array 
          syntax: array.unshift() 
       Shift method 
          removes an element at the beginning of an array
          syntax: array.shift()
        Sort method 
          syntax: array.sort()
        Reverse method 
          syntax. array.reverse()
        Splice and Slice 
          can use in a string and arrays
          splice 
            array.splice() 
            directly manipulates the original array
            returns array of omitted elements 
            parameters
              first parameter: index where to start omitting element
              second parameter: # of elements to be omitted starting from the first parameter
              third paramter: elements to be added in place of the omitted elements
          slice
            does not affect the original array
            first parameter: index where to begin omitting elements
            second paramter - number of elements to be removed, from beginning of the array will count the number of the second parameter and will stop at the number given (index - 1)
        Concat method 
          used to merge two or more arrays 
        Join method 
          used to join elements to one string 
        toString method
          converts numbers to string

*/

let count = ["one", "two", "three", "four"];
console.log(count.length);
console.log(count[4]);

// adding new element to array with assignment operator
count[4] = "five";
console.log(count);

// Push method
count.push("element");
console.log(count);

// function pushMethod() {
//   return count.push("six");
// }
// pushMethod();
// pushMethod();

//can also be used as a function
function pushMethod(element) {
  return count.push(element); //use a parameter for dynamic/changing output
}
pushMethod("six");
pushMethod("seven");
pushMethod("eight");
console.log(count);

//Pop method
count.pop(); //doesn't take a value in the ()
console.log(count);

// can also be used as a function
function popMethod() {
  return count.pop();
}
popMethod();
console.log(count);

//Unshift method
count.unshift("hugot");
console.log(count);

function unshiftMethod(element) {
  return count.unshift(element);
}
unshiftMethod("zero");
console.log(count);

//Shift method
count.shift();
console.log(count);

function shiftMethod() {
  return count.shift();
}
shiftMethod();
console.log(count);

//Sort method
let nums = [15, 32, 61, 130, 230, 13, 34];
nums.sort(); //arranges by the first number only; needs callback function to use the whole number
console.log(nums);

//sort nums in ascending order
nums.sort(function (a, b) {
  //will be invoked with the sort method; no need for name; a = previous value, b = current value
  return a - b; //not subtract; like a to b
});

console.log(nums);

//Reverse method
nums.reverse();
console.log(nums);

// Splice method

/* let newSplice = count.splice(1); //will remove the elements from the index number 1 and above 
console.log(newSplice); //contains the removed elements
console.log(count);*/

/* let newSplice = count.splice(1, 2);
console.log(newSplice);
console.log(count); */

/* let newSplice = count.splice(1, 2, "a1", "b2", "c3");
console.log(newSplice);
console.log(count); */

//Slice method

console.log(count);
/* let newSlice = count.slice(1);
console.log(newSlice);
console.log(count); */

let newSlice = count.slice(1, 5); // will remove elements from 1 index position ('two') and will stop removing at the 5 element number ('five')
console.log(newSlice);

//Concat
console.log(count);
console.log(nums);
let animals = ["bird", "cat", "dog", "fish"];

let newConcat = count.concat(nums, animals);
console.log(newConcat);

//Join
let meal = ["rice", "steak", "juice"];
let newJoin = meal.join(); //can/cannot have parameters; default is comma to join; parameters: " ","-",""
newJoin = meal.join(""); //forms one word
newJoin = meal.join(" "); //puts space between the words
newJoin = meal.join("-"); //puts dash between
console.log(newJoin);

//toString
console.log(nums);
console.log(typeof nums[3]);

let newString = nums.toString();
console.log(newString);

/* 
  Accessors
    indexOf()
      array.indexOf()
      finds the index of a given element where it is 'first' found 
      if the element is non existing will return -1
    lastIndexOf()
      array.lastIndexOf()
      finds the index of a given element where it is 'last' found 
*/

let countries = ["US", "PH", "CAN", "PH", "SG", "HK", "PH", "NZ"];

//indexOf
let index = countries.indexOf("PH");
console.log(index);

//lastIndexOf
let lastIndex = countries.lastIndexOf("PH");
console.log(lastIndex);

//if else with accessors
if (countries.indexOf("CAN") == -1) {
  console.log(`Element not existing`);
} else {
  console.log(`Element exists in the countries array`);
}

/* 
  Iterators 

    forEach(cb()) cb = callback function
      array.forEach()
      used to display elements from our array
      used to iterate 
      returns undefined 
    map()
      array.map()
      returns a copy of the array that can be manipulated 
    filter
      array.filter(cb())
    includes 
      array.includes
      returns boolean values 
    every
      return boolean values
      returns true only if the conditions are passed for every element in the array
      strict
    some
      return boolean values
      returns true if the conditions are passed for some elements in the array
    reduce 
      reduce(cb(previous, current))


*/

let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];

//forEach
let daysWeek = days.forEach(function (element) {
  return element; // will always return undefined
});
console.log(daysWeek);

//map
let mapDays = days.map(function (day) {
  return `${day} is the day of the week`;
});
console.log(mapDays);
console.log(days);

//Mini activity
let days2 = [];
days.forEach(function (element) {
  days2.push(element);
});
console.log(days2);

//filter
console.log(nums);
let newFilter = nums.filter(function (num) {
  return num < 50;
});
console.log(newFilter);

// includes
console.log(animals);
let newIncludes = animals.includes("dog");
console.log(newIncludes);

function miniAct(value) {
  if (animals.includes(value) == true) {
    return `${value} is found`;
  } else {
    return `${value} not found`;
  }
}
console.log(miniAct("whale"));
console.log(miniAct("cat"));
console.log(miniAct("bird"));

let newEvery = nums.every(function (num) {
  return num > 50;
});
console.log(newEvery);

let newSome = nums.some(function (num) {
  return num > 50;
});
console.log(newSome);

let newReduce = nums.reduce(function (a, b) {
  // return b - a;
  return a + b;
});
console.log(newReduce);

//get average of nums array
// total all elements
// divide by total number of elements
let average = newReduce / nums.length;
// toFixed(# of decimals) - returns a string; adjusts number of decimals
console.log(average.toFixed(2));

//parseInt() - converts to a number but removes the decimal - or parseFloat() - converts to a number and keeps the decimal

console.log(parseInt(average.toFixed(2)));
console.log(parseFloat(average.toFixed(2)));
