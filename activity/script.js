let student = [];
let sectionedStudents = [];
function addStudent(studentName) {
  console.log(`${studentName} was added to the student's list.`);
  return student.push(studentName);
}

addStudent("Snoopy");
addStudent("Charlie");
addStudent("Woodstock");
console.log(student);

function countStudents() {
  console.log(`There are a total of ${student.length} students enrolled`);
}
countStudents();

function printStudents() {
  student.sort();
  //loops through all elements of the array printing all the names of the students
  /*   student.forEach(consoleStudent);
  function consoleStudent(item) {
    console.log(item);
  } */
  student.forEach(function (name) {
    console.log(name);
  });
}
printStudents();

// function findStudent(studentName) {
//   student.filter(function (studentName) {
//     return studentName.toLowerCase() != student.toLowerCase();
//   )
// }

/* function findStudent(keyword) {
  let word = keyword.toLowerCase();
  student.filter(function () {
    return student.includes(word);
  });
}

console.log(findStudent("Joy")); */

/* function findStudent(studentName) {
  if (student.includes(studentName) == true) {
    return `${studentName} is an enrollee`;
  } else {
    return `${studentName} is not an enrollee`;
  }
}
console.log(findStudent("Lucy")); */

/* Filter */
function findStudent(keyword) {
  let match = student.filter(function (name) {
    // if the student name includes the keyword provided
    // name parameter/parameters in the function filter: filter will filter through the array so name (in this case) is taking the place of the student array (? pretty sure); the filter is filtering through with the function given and the name/parameter is what is taking the place of the array because it is filtering through every entry/element of the array idk bahala na
    return name.toLowerCase().includes(keyword.toLowerCase()); //needs to be the keyword being input (not name because that is taking the place of the array)
  });

  //if a match was found the array's length will be 1
  if (match.length == 1) {
    console.log(`${match} is an enrollee`);
  } else if (match.length > 1) {
    console.log(`${match} are enrollees`);
  } else {
    // if no match is found the array's length will be 0
    console.log(`No student found with the name ${keyword}`);
  }
}

findStudent("Lucy");

//reassign a value (add section)
function addSection(section) {
  //reasssigns the value of the 'map' method to the sectionedStudents array including the specified text and the section of the student
  sectionedStudents = student.map(function (name) {
    // returns the name of the student with the added text and the section provided as a string
    return name + " - section" + section;
  });
  //prints the array of sectionedStudents for verification
  console.log(sectionedStudents);
}

//remove student name
function removeStudent(name) {
  //converts the first letter of the name to uppercase
  let firstLetter = name.slice(0, 1).toUpperCase;
  //retrieve all other letters of the name
  let remainingLetters = name.slice(1, name.length);
  //combines the capitalized and remaining letters of the name
  let capitalizedName = firstLetter + remainingLetters;
  //Used to retrieve the index of the name provided
  // used to look for a match or to find the name inputted through the index
  let studentIndex = student.indexOf(capitalizedName);
  //if a match is found the index number would be 0 or greater
  // the indexOf method will return a -1 value if the name is not found
  if (studentIndex >= 0) {
    //manipulate the array to remove the element using the index of the name provided
    student.splice(studentIndex, 1);
  }

  console.log(name + "was removed from the student list");
}
